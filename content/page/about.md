---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

I'm Aref Alikhani, from Sari (Mazandaran, Iran). I received my bachelor's degree in Plant Protection in 2015 and my master’s degree in Entomology in 2018, both from Sari Agricultural Sciences University.
These days, I'm working as a researcher at SANRU and of course, trying to learn more about GNU/Linux.